# excutar 'apt-get update'
exec { 'apt-update': # Recursos com este nome serão sempre executados
command => '/usr/bin/apt-get update'
}

# instalar pacote apache2
package { 'apache2':
require => Exec['apt-update'], # exige 'apt-update' antes da instalação
ensure => installed,
}

service { 'apache2':
ensure => running,
}


package { 'mysql-server':
require => Exec['apt-update'], # exige 'apt-update' antes da instalação
ensure => installed,
}

package { 'php5':
require => Exec['apt-update'], # exige 'apt-update' antes da instalação
ensure => installed,
}

file { '/var/www/html/info.php':
ensure => file,
content => '<?php phpinfo(); ?>', # codigo para phpinfo
require => Package['apache2'], # exige o pacote 'apache2' antes de ser criado
}
